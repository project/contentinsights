<?php
/**
 * @file
 * Administration pages for contentinsights.
 */

/**
 * Admin settings form for contentinsights module.
 */
function contentinsights_settings_form($form, &$form_state) {
  $form = array();

  $form['contentinsights_siteid'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Site ID'),
    '#default_value' => variable_get('contentinsights_siteid', ''),
    '#description' => t('The site ID was provided to you by Content Insights.'),
    '#required' => TRUE,
  );

  $form['tracker'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracker integration'),
    '#description' => t('Customize tracking code parameters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $node_sources = array('node' => t('Pull from node'));
  drupal_alter('contentinsights_node_sources', $node_sources);

  $field_sources = array();
  $fields = field_info_fields('node');
  foreach ($fields as $name => $info) {
    if (in_array($info['type'], array('text', 'taxonomy_term_reference'))) {
      $field_sources[$name] = $name;
    }
  }
  drupal_alter('contentinsights_field_sources', $field_sources);

  $form['tracker']['contentinsights_maincontent'] = array(
    '#type' => 'textfield',
    '#title' => t('Main content location'),
    '#description' => t('Element on the page where the main article content is located, for example: <em>#id</em> or <em>.class</em>'),
    '#default_value' => variable_get('contentinsights_maincontent', ''),
  );
  $form['tracker']['contentinsights_title'] = array(
    '#type' => 'select',
    '#options' => $node_sources,
    '#title' => t('Article title'),
    '#default_value' => variable_get('contentinsights_title', 'node'),
  );
  $form['tracker']['contentinsights_pubdate'] = array(
    '#type' => 'select',
    '#options' => $node_sources,
    '#title' => t('Article publication date'),
    '#default_value' => variable_get('contentinsights_pubdate', 'node'),
  );
  $form['tracker']['contentinsights_authors'] = array(
    '#type' => 'select',
    '#options' => $node_sources,
    '#title' => t('Authors'),
    '#description' => t('A single author, or a comma separated list of author names.'),
    '#default_value' => variable_get('contentinsights_authors', 'node'),
  );
  $form['tracker']['contentinsights_sections'] = array(
    '#type' => 'select',
    '#options' => $field_sources,
    '#title' => t('Sections'),
    '#description' => t('Comma separated list of article categories/topics/taxonomies.'),
    '#default_value' => variable_get('contentinsights_sections', ''),
  );
  $form['tracker']['contentinsights_tags'] = array(
    '#type' => 'select',
    '#options' => $field_sources,
    '#title' => t('Tags'),
    '#description' => t('Comma separated list of article tags.'),
    '#default_value' => variable_get('contentinsights_tags', ''),
  );
  $form['tracker']['contentinsights_comments'] = array(
    '#type' => 'select',
    '#options' => $node_sources,
    '#title' => t('Comments'),
    '#default_value' => variable_get('contentinsights_comments', 'node'),
  );

  return system_settings_form($form);
}
